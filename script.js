'use strict';


// 1) change background color for paragraph
let paragraph = document.body.querySelectorAll('p');
paragraph.forEach((item) => {
    item.style.backgroundColor ="#ff0000"
});


// 2) find el with  id="optionsList"; find parent && child nodes + childNode type && name

let optionsList = document.getElementById("optionsList");
console.log(optionsList);

console.log(optionsList.parentNode);

if(optionsList.childNodes!==0){
   let childType = optionsList.childNodes;

    childType.forEach((child) => {
    console.log(`This is child nodeType:  ${child.nodeType}`);
    console.log(`This is child nodeName:  ${child.nodeName}`);
}) 

    //// or
    // for (const child of childType) {
    //     console.log(`This is child nodeType:  ${child.nodeType}`);
    //     console.log(`This is child nodeName:  ${child.nodeName}`);
    // }

} else console.log("No child Nodes");




// 3) add new content to <p> with class = "testParagraph"

let testParagraph = document.getElementById("testParagraph");
testParagraph.innerText = "This is a paragraph";


// 4) find all elements in the header;  add a new class="nav-item" for all these elements

let header = document.querySelector(".main-header");
let headerElements = header.querySelectorAll("*");

console.log(headerElements);

headerElements.forEach((item) =>{
    item.classList.add("nav-item");
});

console.log(headerElements);

// 5) search all el with class = "section-title" and delete this class

let sectionTitle = document.querySelectorAll(".section-title");
console.log(sectionTitle);

sectionTitle.forEach((item) =>{
    item.classList.remove("section-title");
    // // or
    // item.classList.toggle("section-title");
})

console.log(sectionTitle);