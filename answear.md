# DOM

это дерево объектов, так браузер представляет html и все что в нем находится
с помощью js можно взаимодействовать

# innerHTML innerText

- innerHTML позваляет добавлять текст с атрибутами HTML
например: 

let example = document.querySelector("p.class")
example.innerHTML = <p style=" color: green;"><b>Hallo world</b></p>
таким образом мы запишем (и/или перезапишем) текст "Hallo world" в зеленом цвете, жирным, при чем атрибут <p> и стиль не будут отображены на странице

- innerText позвошляет только добавить текст
пример:
let example = document.querrySelector("p.class")
example.innerHTML =  "Hallo world";
здесь запишеться только текст, если мы добавим атрибуты, то они тоже будут отображаться на странице 


# способы как обратиться к элементу на странице и какой самый лучший 

- способы:

1. let idElement = document.getElementById('idElement') - если у элемента есть уникальный id
2. let classNameElement = document.getElementsByClassName('classNameElemen'); - возваращает уоллекцию элементов по классу
3. querySelector() - возвращает первый элемент, который ищем  или querySelectorAll() - возвращает коллекцию элементов
let selectorElement = document.querySelector('#selectorElement'); - возвращает элемент с указанным id
let selectorElement = document.querySelector('.selectorElement');- возвращает элемент с указанным классом
4. let tagNameElement = document.getElementsByTagName("div"): - возвращает коллекуцию с указанным тегом
5. let byNameElement = document.getElementsByName("elementsName"); - возвроащает коллекцию элементов по атрибуту name = "elementsName" ;

если нужно найти все элементы по тегу, лучше использовать getElementsByTagName()

самый универсальный  querySelector() или querySelectorAll(), здесь можно указать тег и конкретный класс, чтоб найти элемент, так же ищет по классу, id